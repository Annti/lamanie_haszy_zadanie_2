# -*- coding: utf-8 -*-
import codecs
import pprint
import re
import hashlib
import itertools


def read_md5_files():
    for i in xrange(1, 4):
        with open(str(i) + ".in") as file_in:
            content_in = file_in.read().strip()
            lines = re.split("\n", content_in)
            for line in lines:
                TO_BREAK[i - 1].add(line)
                TO_BREAK_CHECK[i - 1][line] = ""


def combinations(word):
    yield word

    combinations_odm = itertools.chain(
        itertools.combinations(xrange(len(word)), 1),
        itertools.combinations(xrange(len(word)), 2),
        itertools.combinations(xrange(len(word)), 3)
    )

    for com in combinations_odm:
        newword = list(word)
        for i in com:
            newword[i] = newword[i].upper()
        yield "".join(newword)


def chceck_with_odm():
    with codecs.open('odm.txt', "r", "UTF8") as file_odm:
        content_odm = file_odm.read().strip()
        lines_odm = re.split("\n", content_odm)

        for line_odm in lines_odm:
            value = re.split(r',|\s', line_odm)[0].lower()
            for odm_word in combinations(value):
                md5 = hashlib.md5(odm_word.encode('utf-8')).hexdigest()
                for t_b in xrange(len(TO_BREAK)):
                    if md5 in TO_BREAK[t_b]:
                        TO_BREAK_CHECK[t_b][md5] = odm_word
                        print TO_BREAK_CHECK[t_b][md5]
                        print md5


if __name__ == '__main__':
    TO_BREAK = [set() for s in xrange(4)]
    TO_BREAK_CHECK = [dict() for d in xrange(4)]
    read_md5_files()
    chceck_with_odm()
    pprint.pprint(TO_BREAK_CHECK)
    for file_out in xrange(1, 4):
        with codecs.open('{}.out'.format(str(file_out)), "w+", "UTF8") as out:
            for key in TO_BREAK_CHECK[file_out - 1].keys():
                try:
                    out.write(
                        key + " " + TO_BREAK_CHECK[file_out - 1][key] + "\n")
                except KeyError as err:
                    print "Nie znaleziono klucza: " + err.message
